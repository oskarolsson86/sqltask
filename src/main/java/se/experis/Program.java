package se.experis;

import java.sql.*;
import java.util.*;


// Program that uses the Chinook sqlite. 
public class Program {

    public static void main(String[] args) {
        int inputId;
        boolean input = false;
        if (args.length == 1) {
            input = true;
            inputId = Integer.parseInt(args[0]);
        } else {
            inputId = 0;
        }

        String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
        Connection conn = null;

        ArrayList<Customer> customers = new ArrayList<Customer>();
        List<String> genres =  new ArrayList<String>();

        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            String updatePositionSql = "SELECT COUNT(*) FROM Customer";
            PreparedStatement pstmt = conn.prepareStatement(updatePositionSql);
            ResultSet result = pstmt.executeQuery();
            result.next();

            int min = 1;
            int max = result.getInt(1);

            if (!input) {
                inputId = (int)(Math.random() * (max - min + 1) + min);
            }

            // Prepare Statement that JOINS the customer to all the genre names.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT C.firstName, C.CustomerId, GE.Name FROM Customer C " +
                            " INNER JOIN Invoice I ON (C.CustomerId = I.CustomerId)" +
                            " INNER JOIN InvoiceLine IL ON (I.InvoiceId = IL.InvoiceId) " +
                            " INNER JOIN Track TR ON (IL.TrackId = TR.TrackId) " +
                            " INNER JOIN Genre GE ON (TR.GenreId = GE.GenreId) " +
                            " WHERE C.CustomerId = ?");
            preparedStatement.setInt(1, inputId);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("firstName"),
                                resultSet.getString("Name")
                        ));
            }

            for (Customer customer: customers) {
                genres.add(customer.getFavGenres());
            }
            System.out.println("Name: " + customers.get(1).getFirstName());
            System.out.println("Most liked genre: " + mostCommon(genres));

        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
    }

    // Method that searches the genres and return the most common
    public static <T> T mostCommon(List<T> list) {
        Map<T, Integer> map = new HashMap<>();

        for (T t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Map.Entry<T, Integer> max = null;

        for (Map.Entry<T, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue())
                max = e;
        }

        return max.getKey();
    }
}
