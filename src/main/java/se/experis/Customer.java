package se.experis;

public class Customer {
    private String customerID;
    private String firstName;
    private String favGenres;

    public Customer(String customerID, String firstName, String favGenres) {
        this.customerID = customerID;
        this.firstName = firstName;
        this.favGenres = favGenres;

    }

    public String getCustomerID() {
        return customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFavGenres() {
        return favGenres;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
